import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAuthModule } from './pages/page-auth.module';

const routes: Routes = [
  {
    path: '',
    children: [{ path: '', loadChildren: () => PageAuthModule }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
