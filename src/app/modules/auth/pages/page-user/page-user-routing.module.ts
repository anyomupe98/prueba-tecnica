import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageUserListComponent } from './page-user-list/page-user-list.component';

const routes: Routes = [
  {
    path:'',
    component: PageUserListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageUserRoutingModule {}
