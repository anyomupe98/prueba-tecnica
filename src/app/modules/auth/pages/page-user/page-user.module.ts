import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageUserListComponent } from './page-user-list/page-user-list.component';
import { UserEditModule } from '../../components/user/user-edit/user-edit.module';
import { PageUserRoutingModule } from './page-user-routing.module';

@NgModule({
  declarations: [PageUserListComponent],
  imports: [CommonModule, UserEditModule, PageUserRoutingModule],
  exports: [PageUserListComponent],
})
export class PageUserModule {}
