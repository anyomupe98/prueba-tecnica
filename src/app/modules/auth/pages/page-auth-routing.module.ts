import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageUserModule } from './page-user/page-user.module';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user',
        loadChildren: () => PageUserModule,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageAuthRoutingModule {}
