import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageAuthRoutingModule } from './page-auth-routing.module';

@NgModule({
  imports: [CommonModule, PageAuthRoutingModule],
})
export class PageAuthModule {}
