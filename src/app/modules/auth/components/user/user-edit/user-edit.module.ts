import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit.component';

@NgModule({
  declarations: [UserEditComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [UserEditComponent],
})
export class UserEditModule {}
