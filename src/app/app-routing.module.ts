import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateComponent } from './template/template/template.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'template',
    pathMatch: 'full',
  },
  {
    path: 'template',
    component: TemplateComponent,
    loadChildren: () =>
      import('./modules/modules.module').then((m) => m.ModulesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
